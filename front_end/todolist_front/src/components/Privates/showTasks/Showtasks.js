import { useEffect, useContext } from 'react'
import { useSelector, useDispatch } from 'react-redux';
// import { Link } from 'react-router-dom';
// import {UserContext} from "../../../ContextApi/userContext"
import { getTasks } from '../../../Reducers/TasksReducer'
// import axios from '../../../axiosConfig/axiosConfigdefault'
// import "./showTask.css"

export default function ShowTasks() {
    // const { isUserLoggedIn, token } = useContext(UserContext)

    const {taskState} = useSelector(state => ({
        ...state.TaskReducer
    }) )

    // Ici le state d'une tâche
    // const {oneTaskState} = useSelector(state => ({
    //     ...state.OneTaskShowerReducer
    // }) )


    const dispatch = useDispatch()

    // console.log(isUserLoggedIn)
    
    useEffect(() => {
        if (taskState.length === 0){
            try {
                dispatch(getTasks())

            } catch (error) {
                console.log(error)
            }
        }
        // if (oneTaskState.length === 0){
        //     dispatch(getOneTasks(id))
        // }
    },[taskState.length, dispatch])

    console.log(taskState)
    // const taskDeleter = async (id) => {
    //     await axios.delete(`/api/deletetasks/${id}`)
    //         .then(res => res.data)
    //         .catch(err => console.log(err))
    // }
    
    // const getOneTasks = async (id, token) => {    
    //     await axios.get(`/api/findOnetask/${id}`, {headers: { Authorization: `Bearer ${token}` }})
    //         .then((res) => res.data)
    //         .then( data => {
    //             return data.data
    //         })
    //         .catch((error) => console.log(error.message))
    
    // }
    // const getId = (id) => {console.log(id)}


    return (
        <div className='taskBody'>
            <div className="card border-info mb-3" style={{maxWidth: 90 + "%"}} key={taskState.id}>
                <div className="card-header bg-transparent border-info"><h5>Title: {taskState.title}</h5></div>
                <div className="card-body text-secondary">
                    <h5 className="card-title">Content</h5>
                    <p className="card-text"> {taskState.content} </p>
                </div>
                <div className="card-footer bg-transparent border-info"><strong>Author: </strong>{taskState.author}</div>

                <div>
                    {/* <button className='btn btn-danger' style={{maxWidth: 10 + 'rem'}} onClick={() => {taskDeleter(task.id)}}>supprimer</button> */}
                </div>

                <div>
                    <button className='btn btn-info' style={{maxWidth: 10 + 'rem'}}>
                        {/* <Link to="/private/onetask" state={{id:task.id, title:task.title, content:task.content, autor:task.autor}} >Afficher l'article</Link> */}
                    </button>
                </div>

                <div>
                    <button className='btn btn-success' style={{maxWidth: 10 + 'rem'}}>
                        {/* <Link to="/private/update-task" state={{id:task.id, title:task.title, content:task.content, autor:task.autor}} >Modifier l'article </Link> */}
                    </button>
                </div>

            </div>

        </div>
    )
}



            
// { taskState.map((task) => {
//     return (
//         <div className="card border-info mb-3" style={{maxWidth: 90 + "%"}} key={task.id}>
//             <div className="card-header bg-transparent border-info"><h5>Title: {task.title}</h5></div>
//             <div className="card-body text-secondary">
//                 <h5 className="card-title">Content</h5>
//                 <p className="card-text"> {task.content} </p>
//             </div>
//             <div className="card-footer bg-transparent border-info"><strong>Autor: </strong>{task.autor}</div>

//             <div>
//                 {/* <button className='btn btn-danger' style={{maxWidth: 10 + 'rem'}} onClick={() => {taskDeleter(task.id)}}>supprimer</button> */}
//             </div>
//             <div>
//                 <button className='btn btn-info' style={{maxWidth: 10 + 'rem'}}>
//                     {/* <Link to="/private/onetask" state={{id:task.id, title:task.title, content:task.content, autor:task.autor}} >Afficher l'article</Link> */}
//                 </button>
//             </div>
//             <div>
//                 <button className='btn btn-success' style={{maxWidth: 10 + 'rem'}}>
//                     {/* <Link to="/private/update-task" state={{id:task.id, title:task.title, content:task.content, autor:task.autor}} >Modifier l'article </Link> */}
//                 </button>
//             </div>

//         </div>
// )
// }) }
