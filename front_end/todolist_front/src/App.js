import './App.css';
import ShowTasks from './components/Privates/showTasks/Showtasks'

function App() {
  return (
    <div className="App">
      <ShowTasks/>
    </div>
  );
}

export default App;
