import { createStore, combineReducers, applyMiddleware } from "redux"
import thunk from 'redux-thunk'
import TaskReducer from '../Reducers/TasksReducer';
//import UserReducer from '../Reducer/LoginReducer';

const rootReducer = combineReducers({ TaskReducer});
const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;