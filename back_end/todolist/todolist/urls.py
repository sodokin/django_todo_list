"""todolist URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers


# import tasksuser
# from tasksuser import views
# from tasksapp import views

# router = routers.DefaultRouter()
# router.register(r'users', tasksuser.views.UserViewSet)
# router.register(r'tasksusers', tasksuser.views.TasksUserViewSet)
# router.register(r'alltasks', views.ShowTasksViewSet)
# router.register(r'groups', tasksuser.views.GroupViewSet)
# router.register(r'createtasks', views.CreateTask)

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('', include(router.urls)),
    path('userapi/', include('tasksuser.urls')),
    path('tasksapi/', include('tasksapp.urls')),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

    # path('blog/', include('blog.urls')),
]
