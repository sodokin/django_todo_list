from django.db import models
from django.contrib.auth import get_user_model
from django.template.defaultfilters import slugify
# from django.urls import reverse

User = get_user_model()


class TasksTable(models.Model):
    title = models.CharField(max_length=255, verbose_name="Title")
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    content = models.TextField(verbose_name="Tasks content")
    slug = models.SlugField(max_length=255, unique=True, blank=True)
    published = models.BooleanField(default=False, verbose_name="Published")
    last_updated = models.DateTimeField(auto_now=True)
    created_on = models.DateField(blank=True, null=True)

    class Meta:
        ordering = ['created_on']
        verbose_name = "Task"

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        super().save(*args, **kwargs)


"""
    @property
    def author_or_default(self):
        return self.author.username if self.author else "L'auteur est inconnu"

    def get_absolute_url(self):
        return reverse('blog:home')
"""
