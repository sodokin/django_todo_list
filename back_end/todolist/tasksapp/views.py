# from django.contrib.auth.models import User, Group
from rest_framework import viewsets, generics
from rest_framework import permissions

# from django.views.generic import ListView, CreateView, UpdateView, DeleteView, DetailView

from tasksapp.models import TasksTable
from tasksapp.serializers import TaskSerializer


class ShowTasksViewSet(viewsets.ModelViewSet):

    queryset = TasksTable.objects.all()
    serializer_class = TaskSerializer
    # permission_classes = [permissions.IsAuthenticated]


class DetailsTaskView(generics.RetrieveAPIView):

    queryset = TasksTable.objects.all()
    serializer_class = TaskSerializer
    # permission_classes = [permissions.IsAuthenticated]


class CreateTaskView(generics.CreateAPIView):
    queryset = TasksTable.objects.all()
    serializer_class = TaskSerializer
    # permission_classes = [permissions.IsAuthenticated]


class UpdateTaskView(generics.RetrieveUpdateAPIView):
    queryset = TasksTable.objects.all()
    serializer_class = TaskSerializer
    # permission_classes = [permissions.IsAuthenticated]


class DeleteTaskView(generics.RetrieveDestroyAPIView):
    queryset = TasksTable.objects.all()
    serializer_class = TaskSerializer
    # permission_classes = [permissions.IsAuthenticated]


class ShowTasksView(generics.ListAPIView):
    queryset = TasksTable.objects.all()
    serializer_class = TaskSerializer
    # permission_classes = [permissions.IsAuthenticated]



