# from django.contrib.auth.models import TasksTable
from rest_framework import serializers

from tasksapp.models import TasksTable


class TaskSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TasksTable
        fields = ['url', 'id', 'title', 'author', 'content']


"""
class TaskCreateSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TasksTable
        fields = ['title', 'author', 'content']
"""