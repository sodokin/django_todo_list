from django.urls import path, include
from rest_framework import routers
from .views import CreateTaskView, UpdateTaskView, DeleteTaskView, DetailsTaskView
from tasksapp import views


router = routers.DefaultRouter()
router.register(r'', views.ShowTasksViewSet)


urlpatterns = [
    path('detail/<int:pk>/', DetailsTaskView.as_view(), name="all-tasks-page"),
    path('create/', CreateTaskView.as_view(), name="create_tasks-page"),
    path('updateTasks/<int:pk>/', UpdateTaskView.as_view(), name="update-tasks-page"),
    path('deleteTasks/<int:pk>/', DeleteTaskView.as_view(), name="delete-tasks-page"),
    path('', include(router.urls)),

    # path('blog/', include('blog.urls')),
]
