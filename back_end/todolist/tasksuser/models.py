from django.db import models
from django.template.defaultfilters import slugify
from django.contrib.auth import hashers
# from django.urls import reverse


# Create your models here.

class TasksUser(models.Model):
    lastname = models.CharField(max_length=55, verbose_name="lastname")
    firstname = models.SlugField(max_length=55)
    password = models.CharField(max_length=255, unique=True)
    email = models.EmailField(max_length=255, unique=True)
    last_updated = models.DateTimeField(auto_now=True)
    created_on = models.DateField(blank=True, null=True)

    class Meta:
        ordering = ['-created_on']
        verbose_name = "Tasks user"

    def __str__(self):
        return self.lastname


    """@property
    def author_or_default(self):
        return self.author.username if self.author else "L'auteur est inconnu"

    def get_absolute_url(self):
        return reverse('blog:home')

"""