from django.apps import AppConfig


class TasksuserConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tasksuser'
