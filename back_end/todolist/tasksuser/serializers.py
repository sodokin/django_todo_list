from django.contrib.auth.models import Group, User
from rest_framework import serializers

from tasksuser.models import TasksUser


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('__all__')


class TasksUserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TasksUser
        fields = ['id', 'lastname', 'firstname', 'email', 'password']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']
